//JQuery Module Pattern

// An object literal
var app2 = {
  init: function() {
    app2.functionOne();
  },
  functionOne: function() {}
};
$("document").ready(function() {
  app2.init();

  $(".menu").on("click", function() {
    $(".mobile-menu").slideDown();
  });

  $(".closed").on("click", function() {
    $(".mobile-menu").slideUp();
  });

  $('.nav-group-mo a[href^="#"]').click(function() {
    $("html, body").animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top
      },
      500
    );
    $(".mobile-menu").slideUp();
    return false;
  });
});
