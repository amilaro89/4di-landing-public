//JQuery Module Pattern

// An object literal
var app = {
  init: function() {
    app.functionOne();
  },
  functionOne: function() {}
};
$("document").ready(function() {
  app.init();
  gsap.registerPlugin(CSSRulePlugin);
  var rule = CSSRulePlugin.getRule(".hero:after");

  var tl = gsap.timeline({ delay: 0.5 });
  tl.from(".hero", { duration: 1, x: -30, opacity: 0 })
    .fromTo(
      rule,
      { cssRule: { x: -300, opacity: 0 } },
      { duration: 1, cssRule: { x: 0, opacity: 1 }, ease: "expo.out" },
      "-=.8"
    )
    .fromTo(
      ".hero nav * ",
      { y: 10, opacity: 0 },
      { duration: 1, y: 0, stagger: 0.1, opacity: 1 },
      "-=.8"
    )
    .fromTo(
      ".hero-group * ",
      { y: 10, opacity: 0 },
      { duration: 1, y: 0, stagger: 0.2, opacity: 1 },
      "-=.95"
    )
    .fromTo(
      ".about * ",
      { y: 10, opacity: 0 },
      { duration: 1, y: 0, stagger: 0.2, opacity: 1 },
      "-=.8"
    );
  // .fromTo(
  //   ".intro * ",
  //   { y: 10, opacity: 0 },
  //   { duration: 1, y: 0, stagger: 0.2, opacity: 1 },
  //   "-=.8"
  // );

  $(".menu").on("click", function() {
    $(".mobile-menu").slideDown();
  });

  $(".closed").on("click", function() {
    $(".mobile-menu").slideUp();
  });

  $('.nav-group-mo a[href^="#"]').click(function() {
    $("html, body").animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top
      },
      500
    );
    $(".mobile-menu").slideUp();
    return false;
  });
});
